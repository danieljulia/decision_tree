'use strict';
  //alert(base_url+'/partials/all.html');
var myApp = angular.module('myApp', [
"ngRoute"
]);


/*

myApp.config(function($routeProvider) {

     $routeProvider.when(
        '/view',
        {
            templateUrl:'templates/table.html',
            controller: 'Home' //todo compte amb la coma a alguns navegadors
        });

    $routeProvider.otherwise(
        {
            redirectTo: '/view'
        });
});

*/

/*
myApp.factory('activitats', function($http,$compile,$location,$timeout){
  var activitats={acts:{}};
  var page=1;
  var markers=new Array();
  var current_loc=null;
  var current_loc_marker=undefined;
  var first=new Date('2017/06/2'); //todo configurable (o data actual?)
  var loc={};
  var inputChangedPromise;
  var inputChangedPromise2;
  activitats.key_tipus={21:'voluntariat',22:'sortida',23:'exposicio',24:'curs',25:'concurs',67:'altres'};

  activitats.loading=true;
  activitats.current_day;
  activitats.current_days=new Array();
  activitats.acts=new Array();
  activitats.paraula="";
    activitats.location="";
  activitats.avis="";
  activitats.msg="";
  activitats.tipus={21:false,22:false,23:false,24:false,25:false,67:false};
  activitats.ppublic={26:false,27:false,68:false,29:false,69:false};
 activitats.tipus_enabled=false;
   activitats.public_enabled=false;
   activitats.mode="";
   var fuse_options = {
  shouldSort: true,
  threshold: 0.5, //0.6
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 1
};



  generateDays();

  loadPage();
  activitats.avis="carregant...";

  function loadPage(){
    var per_page=100;
    //carregar activitats de REST WP
    $http({
      method: 'GET',
      url: base_url+'/wp-json/wp/v2/sn_event?per_page='+per_page+'&filter[orderby]=meta_value_num&filter[meta_key]=sn_inici&order=desc&page='+page   //todo.. han de ser més!!
    }).then(function successCallback(response) {

        activitats.avis="";

        activitats.loading=false;
    //  activitats.total=response.data.length;
        var now = new Date();

        for(var i=0;i<response.data.length;i++){
          var act=response.data[i];

          var d=new Date(act.inici*1000);

          response.data[i].inici= d;
          response.data[i].order= d.getTime();
          //response.data[i].old= false;


          if(act.final!=""){
            response.data[i].final= new Date(act.final*1000);
          }else{

          }
          response.data[i].visible=true;
          //  activitats.acts[i].c=$compile(activitats.acts[i].cats);
          //  console.log(act.inici);
        }
          activitats.acts=activitats.acts.concat(response.data);
          //console.log("total",activitats.acts.length);
            activitats.total="carregant ..."+activitats.acts.length;

          if(response.data.length==per_page){
            page++;
            loadPage();
          }else{
            console.log("*** ordenant per order");
              activitats.total=activitats.acts.length;
            //ordenar per data per defecte
            activitats.acts.sort(function(a, b) {
              return a.order - b.order;
          });
            setTimeout(function(){
              activitats.createMarkers();
            },500);

          }
      }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
    });
  }



activitats.changeView=function(view){
  activitats.view=view;
  current_loc=null;
  current_loc_marker=null;
  $location.path(view); // path not hash

}

activitats.isSelected = function(view){
    if(activitats.view==view){

      return "selected";
    }
    return "";
}

activitats.toggleTipus=function(num){
  current_loc=null;
  activitats.tipus_enabled=true;
  activitats.tipus[num]=!activitats.tipus[num];
  activitats.filter();
}

  activitats.esTipus=function(num){
    if(!activitats.tipus_enabled) return "disabled";

    if(activitats.tipus[num]) return "selected";
    return "not-selected";

  }

  activitats.togglePublic=function(num){
    current_loc=null;
    activitats.public_enabled=true;
    activitats.ppublic[num]=!activitats.ppublic[num];
    activitats.filter();
  }

  activitats.esPublic=function(num){
    if(!activitats.public_enabled) return "disabled";
    if(activitats.ppublic[num]) return "selected";
    return "not-selected";

  }

  activitats.dosort=function(tipus){

    switch(tipus){
      case "nom":
          //  Object.keys($scope.acts).sort(function(a, b) {


          activitats.acts.sort(function(a,b){
              return a.title.rendered.toLowerCase().localeCompare(b.title.rendered.toLowerCase());
            //  return a.title.rendered<b.title.rendered;
          });
            break;
      case "lloc":

        activitats.acts.sort(function(a,b){
        return a.loc.toLowerCase().localeCompare(b.loc.toLowerCase());
      });

            break;
      case "data":


          for(var i=0;i<activitats.acts.length;i++){
             activitats.acts[i].order=activitats.acts[i].inici.getTime();
           }
           activitats.filter();


            break;
          }




  }

  activitats.search=function(){

    if(inputChangedPromise){
        console.log("cancelant...");
        $timeout.cancel(inputChangedPromise);
    }
    inputChangedPromise = $timeout(function(){
      console.log("executant...");
      activitats.filter();
    },500);



  }



  activitats.filter=function(loc){

    if(loc==undefined) current_loc=null;
        if(loc=="") current_loc=null;
    if(current_loc==null){
          if(window.map) map.setZoom(8);
      if(current_loc_marker!=null)
        current_loc_marker.setVisible(false);
    }
    activitats.avis="carregant..";
    var res=new Array();


    //fer totes invisibles
    for(var i=0;i<this.acts.length;i++){
      var act=this.acts[i];
      act.visible=false;
      act.ft=false;
      act.fp=false;
      act.fd=false;
      act.fpa=false;
      act.flo=false;
       act.order=act.inici.getTime();
    }

    //filtrar per localitzacio


    if(current_loc){
      var pas=0.35;
      for(var i=0;i<activitats.acts.length;i++){
        a=activitats.acts[i];
        if(a.location.latitude>current_loc.lat-pas){
          if(a.location.latitude<current_loc.lat+pas){
            if(a.location.longitude<current_loc.lng+pas){
              if(a.location.longitude>current_loc.lng-pas){
                a.flo=true;
                var d=Math.sqrt( (a.location.latitude-current_loc.lat)*(a.location.latitude-current_loc.lat)+
                (a.location.longitude-current_loc.lng)*(a.location.longitude-current_loc.lng) );
                 a.order=d;

              }//end if

            }//end if

          }//end if
        }//end if

      }//end for

        var t=postProcessLocalitzacio();
        if(t>0){
            activitats.avis="";
            return;
          }
      }



    //filtrar per paraula
    var paraula=activitats.paraula.toLowerCase();

    if(paraula!=""){

      fuse_options.keys= [
        "title.rendered"
      //  "teaser"
      ];
      var fuse = new Fuse(activitats.acts, fuse_options); // "list" is the item array
      var result = fuse.search(paraula);


      var a;
      for(var i=0;i<activitats.acts.length;i++){
        a=activitats.acts[i];

        for(var j=0;j<result.length;j++){
          if(result[j].slug==a.slug){
            a.fpa=true;
             a.order=j;
          }
        }
      }



        var t=postProcessParaula();
        if(t>0){
          activitats.avis="";
          return;
        }
    }else{
      activitats.mode="";
    }





    //filtrar per tipus i public

    var l=this.acts.length;
    for(var i=0;i<l;i++){
      var act=this.acts[i];
      //si és d'alguna de les categories actuals

      //tipus
      for (var key in this.tipus) {
        if(this.tipus[key]){
          for(var k=0;k<act.tipus.length;k++){
            if(act.tipus[k]==key){
              act.ft=true;
            }
          }
        }
      }

      //public
      for (var key in this.ppublic) {
        if(this.ppublic[key]){
          for(var k=0;k<act.public.length;k++){
            if(act.public[k]==key){
              act.fp=true;
            }
          }
        }
      }
    }



    //filtrar per dia
    if(!daysEmpty()){

    var undia=1000*24*60*60;

    for (var day in activitats.current_days) {
      if(activitats.current_days[day]){
      var t0=new Date(day).getTime();
      for(var i=0;i<activitats.acts.length;i++){
        var act=activitats.acts[i];
        //console.log("filtrant activitats",act,act.inici.getTime());
        if(act.inici.getTime()>t0 && act.inici.getTime()<(t0+undia) ){
          act.fd=true;
        }else if( act.final!="" ){
          if( act.inici.getTime()<= (t0) && act.final.getTime()>= (t0) ){
            act.fd=true;
          }
        }
          }//end for activitats
        }//end si filtre dia actiu
      }//end per cada un dels dies marcats
    }else{

    }





    //fi filtre per dies

    postProcessFiltres();


    var hide = function() {
           activitats.avis="";
       }

       $timeout(hide, 500);


  }

function postProcessParaula(){
  var total=0;
  for(var i=0;i<activitats.acts.length;i++){
    var act=activitats.acts[i];
    if( act.fpa ){
      act.visible=true;
      total++;

    }
  }

  if(total==0){
    activitats.tipus_enabled=true;
    activitats.public_enabled=true;
    activitats.mode="";
    //activitats.current_day=null;
  }else{
    activitats.tipus_enabled=false;
    activitats.public_enabled=false;
    activitats.current_day=null;

    activitats.mode="paraula '"+activitats.paraula+"'";
  }
  activitats.total=total;


  activitats.acts.sort(function(a, b) {
    return a.order - b.order;
})

  activitats.refreshMap();
  return total;

}
function postProcessLocalitzacio(){
  var total=0;
  for(var i=0;i<activitats.acts.length;i++){
    var act=activitats.acts[i];
    if( act.flo ){
      act.visible=true;
      total++;

    }
  }

  if(total==0){
    activitats.tipus_enabled=true;
    activitats.public_enabled=true;
    activitats.mode="";
    //activitats.current_day=null;
  }else{
    activitats.tipus_enabled=false;
    activitats.public_enabled=false;
    activitats.current_day=null;

    activitats.mode="localització '"+activitats.location+"'";
  }
  activitats.total=total;

  activitats.acts.sort(function(a, b) {
    return a.order - b.order;
})
  activitats.refreshMap();

  return total;

}

function postProcessFiltres(){
  //si cumpleix tots els filtres fer visible
  var total=0;
  for(var i=0;i<activitats.acts.length;i++){

      var act=activitats.acts[i];
      if(
        (act.ft || !activitats.tipus_enabled) &&
        (act.fp || !activitats.public_enabled) &&
        (act.fd || daysEmpty())
      ){
        act.visible=true;
        total++;
      }

    }
  activitats.total=total;

  //en aquest cas s'ordenen per data

  activitats.acts.sort(function(a, b) {
    return a.order - b.order;
})

  activitats.refreshMap();
  return total;
}


function existsLoc(a){
  var l=activitats.acts.length;

  for(var i=0;i<l;i++){
      var act=activitats.acts[i];
      if(act!=a){
        if (act.location.latitude==a.location.latitude){
            if (act.location.longitude==a.location.longitude){
              return true;

            }
        }
      }
    }
    return false;
}

  activitats.createMarkers=function(){
    markers=new Array();
    var a;
    var l=activitats.acts.length;

    for(var i=0;i<l;i++){
      a=activitats.acts[i];
    //  console.log(a);
      var tipus="altres";
      if(a.tipus.length==0){
        error("no hi ha tipus assignat a ",a);
      }else{
        tipus=activitats.key_tipus[a.tipus[0]];
        //console.log(a,a.tipus);
      //  tipus=a[Object.keys(a.tipus)[0]];
      }

      if(existsLoc(a)){
        var pas=0.005;
        a.location.latitude=parseFloat(a.location.latitude)+Math.random()*pas-pas/2;
        a.location.longitude=parseFloat(a.location.longitude)+Math.random()*pas-pas/2;
        //console.log("he canviat la posició d'aquest marcador",a.title.rendered);

      }
      //a.title.rendered
      var m=createMarker(
        i,"<article class='resultats_una_activitat'>"+a.teaser+"</article>",
        {lat:parseFloat(a.location.latitude),lng:parseFloat(a.location.longitude)},
      tipus);
      m.a=a;
      markers.push(m);

    }

  }

  activitats.refreshMap=function(){

  //  console.log("refreshMap",markers.length, activitats.acts);
    if(window.current_infowindow) current_infowindow.close();
    for(var i=0;i<markers.length;i++){
    //  var v=false;
    //  if(activitats.acts[i].visible!=undefined) v=activitats.acts[i].visible;
       markers[i].setVisible(markers[i].a.visible);
    }
  }

  activitats.geoCode=function(){

        if(activitats.location==""){
          current_loc=null;
          activitats.filter();
        return;
      }
    activitats.avis="carregant..";
      var place=activitats.location.toLowerCase();
    place+=",%20Spain";
    //console.log("buscant per ",place);
    var url="https://maps.googleapis.com/maps/api/geocode/json?address="+place+"&key="+gmaps_api_key;
    $http({
      method: 'GET',
      url: url
    }).then(function successCallback(response) {

        var first=response.data.results[0];
        //formatted_address
          loc.lat=first.geometry.location.lat;
        loc.lng=first.geometry.location.lng;
        map.setCenter(loc);
        map.setZoom(10);
        current_loc=loc;
      //  console.log("trobada localitzacio ",loc,response.data,"current_loc.png");
      if(current_loc_marker==undefined){
        current_loc_marker=createMarker(0,"Posició de cerca",current_loc,0,"current_loc.png");
      }else{
        current_loc_marker.setPosition(current_loc);
        current_loc_marker.setVisible(true);
      }
        activitats.filter(true);
      //  activitats.avis="";
        //todo calcular en funció d'això els resultats que toquen
      }, function errorCallback(response) {

    });

  }

function showAll(){
  for(var i=0;i<activitats.acts.length;i++){
    activitats.acts[i].visible=true;
  }
}
  function generateDays(){
    activitats.days=new Array();

    var mes=["Gen","Feb","Mar","Abr","Maig","Juny","Jul","Ago","Set","Oct","Nov","Dec"];
    var dies=["Dium","Dill","Dim","Dix","Dij","Div","Diss","Dium"];

    for(var i=0;i<10;i++){
      var d = new Date(first.getTime());

      d.setDate(first.getDate()+i);
        d.setHours(0,0,0,0);
      //d.setMonth(first.getMonth());
      //d.setYear(first.getYear());
      d.diasetmana=dies[d.getDay()];
      d.mes=mes[d.getMonth()];
      activitats.days.push(d);
    }
  //  console.log(activitats.days);
    jQuery('ul.opcions_cerca_data').show('fast');

  }



  activitats.next=function(){
    //  var d = new Date();

    first.setDate(first.getDate()+7);
    generateDays();
  }
  activitats.prev=function(){
    //  var d = new Date();
    first.setDate(first.getDate()-7);
      generateDays();
  }
  activitats.day=function(d){

    activitats.paraula="";
    activitats.location="";
    if(  activitats.current_days[d]==undefined){
        activitats.current_days[d]=false;
    }
    activitats.current_days[d]=!activitats.current_days[d];


    //revisar si tots estan a false.. llavors anular filtre
//    console.log("current days",activitats.current_days);

    if(activitats.current_day==d){
      activitats.current_day=null;

    }else{
      activitats.current_day=d;
  }
    activitats.filter();
  }

  function daysEmpty(){
    var seleccionats=0;
    for (var day in activitats.current_days) {
          if(activitats.current_days[day]) seleccionats++;
      }
      if(seleccionats==0){
        return true;
      }
      return false;
  }
  activitats.isDay=function(d){

    if(daysEmpty()) return "opcions_cerca_dia  disabled";

    for (var day in activitats.current_days) {
        if( new Date(day).getTime()==d.getTime()){
          if(activitats.current_days[day]) return "opcions_cerca_dia selected";
          else return "opcions_cerca_dia  not-selected";
        }
      }
return "opcions_cerca_dia  not-selected";

  }
  return activitats;
});
*/

function error(msg){
  console.log(msg);
}

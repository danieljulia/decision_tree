'use strict';


var data={
  'cata':{'header':'header','cat1':'Categoria 1','cat2':'Categoria 2','cat3':'Categoria 3','cat4':'Categoria 4'},
  'catb':{'header':'header','catb1':'Categoria 1','catb2':'Categoria 2','catb3':'Categoria 3','catb4':'Categoria 4'},
  'docs':[
    {'name':'blah','cata':['cat1','cat2','cat3'],'catb':['catb1']},
    {'name':'blah2','cata':['cat1','cat2','cat3'],'catb':['catb1']},
    {'name':'bla3','cata':['cat2','cat3'],'catb':['catb1']},
    {'name':'blah4','cata':['cat3'],'catb':['catb1']},
    {'name':'blah5','cata':['cat2','cat4'],'catb':['catb1','catb2']},
    {'name':'blah6','cata':['cat3','cat3'],'catb':['catb1']},
    {'name':'blah7','cata':['cat1','cat2','cat3'],'catb':['catb1']},
    {'name':'blah8','cata':['cat2','cat3'],'catb':['catb1','catb3','catb4']},
    {'name':'blah9','cata':['cat1','cat2','cat3'],'catb':['catb1']},

  ]
}

var monthDays = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];

myApp.controller("Home" ,function ($scope,$http,$rootScope,$location) {


  $scope.data=data;

//  $scope.cells=

  //calcular amb els filtres actuals quins docs hi ha ha cada cela
  $scope.filter=function(){

  }

  $scope.cell=function(cat1,cat2){
    if(cat1!="header" && cat2!="header"){
      var docs=getDocs(cat1,cat2);
      return docs.length;
    }else{
      return "header";
    }
  }


  //prova calendari
  var dates = [];
    for (var i = 0; i < monthDays.length; i++ ) {
        if (i % 7 == 0) dates.push([]);
        dates[dates.length-1].push(monthDays[i]);
    }
  $scope.dates = dates;

  function getDocs(cat1,cat2){
    var res=[];
    for(var i=0;i<$scope.data.docs.length;i++){
      var doc=$scope.data.docs[i];
      if(docHasCata(doc,cat1)){
        if(docHasCatb(doc,cat2)){
            res.push(doc);
        }
      }
    }
    return res;
  }

  function docHasCata(doc,cat){
    for (var i=0;i<doc.cata.length;i++){
      console.log(doc.cata[i],cat);
       if(doc.cata[i]==cat) return true;
   }
   return false;
  }

  //todo DRY!
  function docHasCatb(doc,cat){
    for (var i=0;i<doc.catb.length;i++){
       if(doc.catb[i]==cat) return true;
   }
   return false;
  }

});
